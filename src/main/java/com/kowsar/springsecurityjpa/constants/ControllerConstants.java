package com.kowsar.springsecurityjpa.constants;

public interface ControllerConstants {

    String BASE_URL = "/api";
    String TUTORIALS_CREATE = "/tutorials";


    String TUTORIALS_GET_ALL = "/tutorials";
    String TUTORIALS_GET_BY_ID = "/tutorials/{id}";
    String TUTORIALS_UPDATE = "/tutorials/{id}";
    String TUTORIALS_DELETE = "/tutorials/{id}";
    String TUTORIALS_DELETE_ALL = "/tutorials";
    String TUTORIALS_GET_PUBLISHED = "/tutorials/published";
    String TUTORIALS_GET_TITLE_CONTAINS = "/tutorials";

}
