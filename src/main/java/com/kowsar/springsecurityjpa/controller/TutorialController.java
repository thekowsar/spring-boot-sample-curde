package com.kowsar.springsecurityjpa.controller;

import com.kowsar.springsecurityjpa.Service.TutorialService;
import com.kowsar.springsecurityjpa.dto.TutorialCreateRequestDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.kowsar.springsecurityjpa.constants.ControllerConstants.*;

@Slf4j
@RestController
@RequestMapping(value = BASE_URL)
public class TutorialController {

    @Autowired
    TutorialService tutorialService;

    @PostMapping(value = TUTORIALS_CREATE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  create(@RequestBody TutorialCreateRequestDto request) throws Exception {
        log.info("Request received for {}", TUTORIALS_CREATE);
        Object response = tutorialService.create(request);
        log.info("Response return {} for {}", TUTORIALS_CREATE, response);
        return response;
    }

    @GetMapping(value = TUTORIALS_GET_ALL, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  get(@RequestParam(required = false) String title) throws Exception {
        log.info("Request received for {}", TUTORIALS_GET_ALL);
        Object response = tutorialService.get(title);
        log.info("Response return {} for {}", TUTORIALS_GET_ALL, response);
        return response;
    }

    @GetMapping(value = TUTORIALS_GET_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  get(@PathVariable Long id) throws Exception {
        log.info("Request received for {}", TUTORIALS_GET_BY_ID + ": " + id);
        Object response = tutorialService.getById(id);
        log.info("Response return {} for {}", TUTORIALS_GET_BY_ID, response);
        return response;
    }

    @GetMapping(value = TUTORIALS_GET_PUBLISHED, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  getAllPublished() throws Exception {
        log.info("Request received for {}", TUTORIALS_GET_PUBLISHED);
        Object response = tutorialService.getAllPublished();
        log.info("Response return {} for {}", TUTORIALS_GET_PUBLISHED, response);
        return response;
    }

    @PutMapping(value = TUTORIALS_UPDATE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  updateById(@PathVariable Long id, @RequestBody TutorialCreateRequestDto request) throws Exception {
        log.info("Request received for {}", TUTORIALS_UPDATE + ": " + id);
        Object response = tutorialService.updateById(request, id);
        log.info("Response return {} for {}", TUTORIALS_UPDATE, response);
        return response;
    }

    @DeleteMapping(value = TUTORIALS_DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  updateById(@PathVariable Long id) throws Exception {
        log.info("Request received for {}", TUTORIALS_DELETE + ": " + id);
        Object response = tutorialService.deleteById(id);
        log.info("Response return {} for {}", TUTORIALS_DELETE, response);
        return response;
    }

    @DeleteMapping(value = TUTORIALS_DELETE_ALL, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object  updateById() throws Exception {
        log.info("Request received for {}", TUTORIALS_DELETE_ALL);
        Object response = tutorialService.deleteAll();
        log.info("Response return {} for {}", TUTORIALS_DELETE_ALL, response);
        return response;
    }

}
