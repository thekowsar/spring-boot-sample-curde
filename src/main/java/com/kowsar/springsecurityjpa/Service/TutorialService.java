package com.kowsar.springsecurityjpa.Service;

import com.kowsar.springsecurityjpa.Repository.TutorialRepository;
import com.kowsar.springsecurityjpa.dto.ResponseDto;
import com.kowsar.springsecurityjpa.dto.TutorialCreateRequestDto;
import com.kowsar.springsecurityjpa.dto.TutorialCreateResponseDto;
import com.kowsar.springsecurityjpa.model.ResponseStatus;
import com.kowsar.springsecurityjpa.model.Tutorials;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TutorialService {

    @Autowired
    TutorialRepository tutorialRepository;

    public Object create(TutorialCreateRequestDto request) throws Exception {
        try{
            Tutorials tutorials = new Tutorials();
            BeanUtils.copyProperties(request, tutorials);
            tutorials = tutorialRepository.save(tutorials);
            return tutorials;
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> create ", e);
            throw new Exception(e);
        }

    }

    public Object get(String title) throws Exception {
        try{
            List<Tutorials> tutorials = tutorialRepository.findByTitileOrAll(title);
            List<TutorialCreateResponseDto> tutorialsResponse = tutorials.stream().map( o ->
                TutorialCreateResponseDto.builder()
                        .id(o.getId())
                        .title(o.getTitle())
                        .description(o.getDescription())
                        .published(o.isPublished())
                        .build()
            ).collect(Collectors.toList());
            return tutorialsResponse;
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> get ", e);
            throw new Exception(e);
        }
    }

    public Object getById(Long id) throws Exception {
        try{
            Optional<Tutorials> tutorial = tutorialRepository.findById(id);
            if(tutorial.isPresent()){
                TutorialCreateResponseDto response = new TutorialCreateResponseDto();
                BeanUtils.copyProperties(tutorial.get(), response);
                return response;
            }
            return "Data Not Found";
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> get ", e);
            throw new Exception(e);
        }
    }

    public Object getAllPublished() throws Exception {
        try{
            List<Tutorials> tutorials = tutorialRepository.findByPublishedTrue();
            List<TutorialCreateResponseDto> tutorialsResponse = tutorials.stream().map( o ->
                    TutorialCreateResponseDto.builder()
                            .id(o.getId())
                            .title(o.getTitle())
                            .description(o.getDescription())
                            .published(o.isPublished())
                            .build()
            ).collect(Collectors.toList());
            return tutorialsResponse;
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> get ", e);
            throw new Exception(e);
        }
    }

    public Object updateById(TutorialCreateRequestDto request, Long id) throws Exception {
        try{
            Optional<Tutorials> tutorial = tutorialRepository.findById(id);
            if(tutorial.isPresent()){
                Tutorials dbObject = tutorial.get();
                dbObject.setTitle(request.getTitle());
                dbObject.setDescription(request.getDescription());
                dbObject.setPublished(request.isPublished());
                tutorialRepository.save(dbObject);
                return dbObject;
            }
            return "Data Not Found";
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> get ", e);
            throw new Exception(e);
        }
    }

    public Object deleteById(Long id) throws Exception {
        try{
            tutorialRepository.deleteById(id);
            return 1;
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> deleteById ", e);
            throw new Exception(e);
        }
    }

    public Object deleteAll() throws Exception {
        try{
            tutorialRepository.deleteAll();
            return 1;
        }
        catch (Exception e) {
            log.error("An Exception occurred during -> deleteAll ", e);
            throw new Exception(e);
        }
    }

}
