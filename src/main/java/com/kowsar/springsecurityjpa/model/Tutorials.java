package com.kowsar.springsecurityjpa.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Tutorials")
@Getter
@Setter
public class Tutorials {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 300)
    private String description;

    private boolean published;

    @Column(length = 100)
    private String title;
}
