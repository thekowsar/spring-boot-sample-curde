package com.kowsar.springsecurityjpa.Repository;

import com.kowsar.springsecurityjpa.model.Tutorials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TutorialRepository extends JpaRepository<Tutorials, Long> {

    List<Tutorials> findByPublishedTrue();

    @Query("SELECT t from  Tutorials t where 1 = 1 " +
            "AND (:title is null or :title = '' or lower(t.title) like lower(concat('%', :title, '%')))" +
            "order by t.title")
    List<Tutorials> findByTitileOrAll(@Param("title") String title);

}
