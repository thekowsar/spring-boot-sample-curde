package com.kowsar.springsecurityjpa.dto;

import com.kowsar.springsecurityjpa.model.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TutorialCreateRequestDto implements Serializable {

    private Long id;

    private String description;

    private boolean published;

    private String title;

}
