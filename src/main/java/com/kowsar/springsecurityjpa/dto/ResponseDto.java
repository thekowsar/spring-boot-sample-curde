package com.kowsar.springsecurityjpa.dto;

import com.kowsar.springsecurityjpa.model.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDto<T> implements Serializable {

    private T data;

}
