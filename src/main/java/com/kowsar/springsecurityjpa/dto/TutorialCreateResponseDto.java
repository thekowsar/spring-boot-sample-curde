package com.kowsar.springsecurityjpa.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TutorialCreateResponseDto implements Serializable {

    private Long id;

    private String description;

    private boolean published;

    private String title;

}
